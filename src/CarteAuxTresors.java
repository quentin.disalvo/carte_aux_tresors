import java.io.IOException;
import java.util.Scanner;

import classes.ExploreMap;
import classes.ParserFile;

public class CarteAuxTresors {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter path to your input file");

        String pathInput = scanner.nextLine();
        System.out.println("Your input path is: " + pathInput);

        System.out.println("Enter path to your output file");

        String pathOutput = scanner.nextLine(); // Read user input
        System.out.println("Your output Path is: " + pathOutput);

        ParserFile parser = new ParserFile();
        ExploreMap explore2;
        System.out.println("Starting exploring: ");
        try {
            explore2 = parser.readFile(pathInput);
            explore2.explore();
            parser.writeFile(pathOutput, explore2);
            System.out.println("Exploration is done, check the results in the file: " + pathOutput);
        } catch (IOException e) {
            System.out.println("Exception during execution:");
            e.printStackTrace();
        }
    }
}
