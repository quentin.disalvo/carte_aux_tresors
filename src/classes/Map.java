package classes;

import java.util.List;

public class Map {
    private int height;
    private int width;
    private List<Coordinate> mountains;
    private List<Treasure> treasures;

    public Map() {

    }

    public Map(int width, int height, List<Coordinate> mountains, List<Treasure> treasures) {
        this.height = height;
        this.width = width;
        this.mountains = mountains;
        this.treasures = treasures;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public List<Coordinate> getMountain() {
        return mountains;
    }

    public void setMountain(List<Coordinate> mountain) {
        this.mountains = mountain;
    }

    public List<Treasure> getTreasures() {
        return treasures;
    }

    public void setTreasures(List<Treasure> treasures) {
        this.treasures = treasures;
    }

}
