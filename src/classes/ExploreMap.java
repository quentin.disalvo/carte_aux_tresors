package classes;

import java.util.ArrayList;
import java.util.List;

import enums.Movement;

public class ExploreMap {
    private Map map;
    private List<Adventurer> adventurers;

    public ExploreMap() {
    };

    public ExploreMap(Map map, List<Adventurer> adventurers) {
        this.map = map;
        this.adventurers = adventurers;
    }

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    public List<Adventurer> getAdventurers() {
        return adventurers;
    }

    public void setAdventurers(List<Adventurer> adventurers) {
        this.adventurers = adventurers;
    }

    public void moveAdventurer(Movement movement, Adventurer adventurer) {
        adventurer.setLastPosition(new Coordinate(adventurer.getCoordinate()));
        adventurer.move(movement);

        // after the classic Human move we check the adventurer conditions
        for (Coordinate mountain : this.map.getMountain()) {
            // if we are on a mountain we can't move
            if (mountain.equals(adventurer.getCoordinate())) {
                adventurer.setCoordinate(adventurer.getLastPosition());
            }
        }

        collectTreasure(adventurer);

        for (Adventurer explorer : adventurers) {
            if (explorer.getCoordinate().equals(adventurer.getCoordinate()) && !explorer.equals(adventurer)) {
                adventurer.setCoordinate(adventurer.getLastPosition());
            }
        }
    }

    private int getMaxSizeMovements(List<Adventurer> listA) {
        int maxSize = 0;
        for (Adventurer adventurer : listA) {
            if (maxSize < adventurer.getMovements().size()) {
                maxSize = adventurer.getMovements().size();
            }
        }

        return maxSize;
    }

    public void collectTreasure(Adventurer adventurer) {
        for (Treasure treasure : this.map.getTreasures()) {
            if (treasure.getCoordinate().equals(adventurer.getCoordinate())
                    && (((adventurer.getLastTreasureFound() != null)
                            && (!adventurer.getLastTreasureFound().equals(treasure.getCoordinate())
                                    || (adventurer.getLastTreasureFound().equals(treasure.getCoordinate())
                                            && !treasure.getCoordinate().equals(adventurer.getLastPosition()))))

                            || (adventurer.getLastTreasureFound() == null))) {
                if (treasure.getValue() > 0) {
                    adventurer.setTreasures(adventurer.getTreasures() + 1);
                    adventurer.setLastTreasureFound(treasure.getCoordinate());
                    treasure.setValue(treasure.getValue() - 1);
                }
            }
        }
    }

    public void explore() {
        int maxSize = getMaxSizeMovements(adventurers);
        for (int i = 0; i < maxSize; i++) {
            for (Adventurer explorer : adventurers) {
                moveAdventurer(explorer.getMovements().get(i), explorer);
            }
        }
    }

}
