package classes;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import enums.Direction;
import enums.Movement;

public class ParserFile {

    public ExploreMap readFile(String path) throws IOException {
        String file = path;

        BufferedReader reader = new BufferedReader(new FileReader(file));

        ExploreMap explore = new ExploreMap();
        List<Adventurer> adventurers = new ArrayList<>();
        List<Coordinate> mountains = new ArrayList<>();
        List<Treasure> treasures = new ArrayList<>();

        Map map = new Map(0, 0, mountains, treasures);
        for (String currentLine = reader.readLine(); currentLine != null; currentLine = reader.readLine()) {
            String[] linePart = currentLine.split("-");

            switch (linePart[0]) {
                case "C":
                    map.setWidth(Integer.parseInt(linePart[1]) - 1);
                    map.setHeight(Integer.parseInt(linePart[2]) - 1);
                    break;
                case "M":
                    mountains.add(new Coordinate(Integer.parseInt(linePart[1]), Integer.parseInt(linePart[2])));
                    break;
                case "T":
                    treasures.add(
                            new Treasure(new Coordinate(Integer.parseInt(linePart[1]), Integer.parseInt(linePart[2])),
                                    Integer.parseInt(linePart[3])));
                    break;
                case "A":
                    Coordinate c = new Coordinate(Integer.parseInt(linePart[2]), Integer.parseInt(linePart[3]));
                    Direction d = Direction.valueOf(linePart[4]);
                    List<Movement> m = new ArrayList<>();
                    for (char mov : linePart[5].toCharArray()) {
                        m.add(Movement.valueOf(String.valueOf(mov)));
                    }

                    adventurers.add(new Adventurer(linePart[1], map, c, d, m));
                    break;
                case "#":
                    break;
                default:
                    break;
            }
        }
        reader.close();
        explore.setAdventurers(adventurers);
        explore.setMap(map);
        return explore;
    }

    public void writeFile(String path, ExploreMap explore) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter("/Users/quentin/Desktop/output.txt"));

        Map map = explore.getMap();
        List<Adventurer> listA = explore.getAdventurers();

        int width = map.getWidth() + 1;
        int height = map.getHeight() + 1;
        String mapInfo = "C-" + width + "-" + height + "\n";
        writer.write(mapInfo);

        String mountainInfo = "";
        for (Coordinate mountain : map.getMountain()) {
            mountainInfo = "M-" + mountain.getX() + "-" + mountain.getY() + "\n";
            writer.write(mountainInfo);
        }

        String treasureInfo = "";
        for (Treasure treasure : map.getTreasures()) {
            if (treasure.getValue() > 0) {
                treasureInfo = "T-" + treasure.getCoordinate().getX() + "-" + treasure.getCoordinate().getY() + "-"
                        + treasure.getValue() + "\n";
                writer.write(treasureInfo);
            }
        }

        String adventurerInfo = "";
        for (Adventurer adventurer : listA) {
            adventurerInfo = "A-" + adventurer.getName() + "-" + adventurer.getCoordinate().getX() + "-"
                    + adventurer.getCoordinate().getY() + "-" + adventurer.getOrientation().toString() + "-"
                    + adventurer.getTreasures() + "\n";
            writer.write(adventurerInfo);
        }
        writer.close();
    }
}
