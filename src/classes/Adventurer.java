package classes;

import java.util.List;

import enums.Direction;
import enums.Movement;

public class Adventurer extends Human {

    private int treasures = 0;
    private Coordinate lastTreasureFound;
    private Coordinate lastPosition;

    public Adventurer() {
    }

    public Adventurer(String name, Map map, Coordinate coordinate, Direction orientation, List<Movement> movements) {
        super(name, map, coordinate, orientation, movements);
    }

    public int getTreasures() {
        return treasures;
    }

    public void setTreasures(int treasures) {
        this.treasures = treasures;
    }

    public Coordinate getLastTreasureFound() {
        return lastTreasureFound;
    }

    public void setLastTreasureFound(Coordinate lastTreasureFound) {
        this.lastTreasureFound = lastTreasureFound;
    }

    public Coordinate getLastPosition() {
        return lastPosition;
    }

    public void setLastPosition(Coordinate lastPosition) {
        this.lastPosition = lastPosition;
    }

}
