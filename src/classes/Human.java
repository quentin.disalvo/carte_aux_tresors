package classes;

import java.util.List;

import enums.Direction;
import enums.Movement;

public class Human {
    private String name;
    private Map map;
    private Coordinate coordinate;
    private Direction orientation;
    private List<Movement> movements;

    public Human() {

    }

    public Human(String name, Map map, Coordinate coordinate, Direction orientation, List<Movement> movements) {
        this.name = name;
        this.map = map;
        this.coordinate = coordinate;
        this.orientation = orientation;
        this.movements = movements;
    }

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    public Direction getOrientation() {
        return orientation;
    }

    public void setOrientation(Direction orientation) {
        this.orientation = orientation;
    }

    public List<Movement> getMovements() {
        return movements;
    }

    public void setMovements(List<Movement> movements) {
        this.movements = movements;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void move(Movement movement) {
        int currentX = this.coordinate.getX();
        int currentY = this.coordinate.getY();

        // we move the human
        switch (this.orientation) {
            case N:
                switch (movement) {
                    case A:
                        this.coordinate.setY(currentY - 1);
                        break;
                    case D:
                        this.orientation = Direction.E;
                        break;
                    case G:
                        this.orientation = Direction.O;
                        break;
                    default:
                        break;
                }
                break;
            case S:
                switch (movement) {
                    case A:
                        this.coordinate.setY(currentY + 1);
                        break;
                    case D:
                        this.orientation = Direction.O;
                        break;
                    case G:
                        this.orientation = Direction.E;
                        break;
                    default:
                        break;
                }
                break;
            case E:
                switch (movement) {
                    case A:
                        this.coordinate.setX(currentX + 1);
                        break;
                    case D:
                        this.orientation = Direction.S;
                        break;
                    case G:
                        this.orientation = Direction.N;
                        break;
                    default:
                        break;
                }
                break;
            case O:
                switch (movement) {
                    case A:
                        this.coordinate.setX(currentX - 1);
                        break;
                    case D:
                        this.orientation = Direction.N;
                        break;
                    case G:
                        this.orientation = Direction.S;
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;

        }
        // finally we check if the new coordinate is still in the map
        this.checkBoundaries();
    }

    private void checkBoundaries() {
        if (this.coordinate.getX() < 0)
            this.coordinate.setX(0);
        if (this.coordinate.getY() < 0)
            this.coordinate.setY(0);
        if (this.coordinate.getX() > this.map.getWidth())
            this.coordinate.setX(this.map.getWidth());
        if (this.coordinate.getY() > this.map.getHeight())
            this.coordinate.setY(this.map.getHeight());
    }

}
