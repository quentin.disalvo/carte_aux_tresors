package test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import classes.Adventurer;
import classes.Coordinate;
import classes.ExploreMap;
import classes.Map;
import classes.ParserFile;
import classes.Treasure;
import enums.Direction;
import enums.Movement;

public class ParserFile_test {
    List<Movement> movementQ = new ArrayList<>();
    List<Movement> movementM = new ArrayList<>();
    List<Movement> movementE = new ArrayList<>();
    Treasure t1 = new Treasure(new Coordinate(2, 3), 2);
    Coordinate m1 = new Coordinate(1, 2);

    Map m = new Map(3, 4, null, null);

    Adventurer quentin = new Adventurer("Quentin", m, new Coordinate(0, 0), Direction.E, movementQ);
    Adventurer martin = new Adventurer("Martin", m, new Coordinate(1, 1), Direction.E, movementM);
    Adventurer etienne = new Adventurer("Etienne", m, new Coordinate(2, 2), Direction.O, movementE);

    ExploreMap explore = new ExploreMap(m, null);

    @Before
    public void init() {
        List<Coordinate> mountains = new ArrayList<>();
        mountains.add(m1);

        List<Treasure> treasures = new ArrayList<>();
        treasures.add(t1);

        m.setMountain(mountains);
        m.setTreasures(treasures);

        List<Adventurer> listExplorer = new ArrayList<>();
        listExplorer.add(etienne);
        listExplorer.add(quentin);
        listExplorer.add(martin);

        explore.setAdventurers(listExplorer);
    }

    @Test
    public void readFile() throws IOException {
        ParserFile parser = new ParserFile();
        ExploreMap ex = parser.readFile("/Users/quentin/Desktop/input.txt");
        Map m = ex.getMap();
        List<Adventurer> listA = ex.getAdventurers();
        Assert.assertEquals(m.getHeight(), 3);
        Assert.assertEquals(m.getWidth(), 2);
        Assert.assertEquals(m.getMountain().get(0).getX(), 1);
        Assert.assertEquals(m.getMountain().get(0).getY(), 0);
        Assert.assertEquals(m.getTreasures().get(0).getCoordinate().getX(), 0);
        Assert.assertEquals(m.getTreasures().get(0).getCoordinate().getY(), 3);
        Assert.assertEquals(m.getTreasures().get(0).getValue(), 2);

        Assert.assertEquals(listA.get(0).getName(), "Lara");
        Assert.assertEquals(listA.get(0).getOrientation(), Direction.S);
        Assert.assertEquals(listA.get(0).getCoordinate().getX(), 1);
        Assert.assertEquals(listA.get(0).getCoordinate().getY(), 1);

        List<Movement> movementL = new ArrayList<>();
        movementL.add(Movement.A);
        movementL.add(Movement.A);
        movementL.add(Movement.D);
        movementL.add(Movement.A);
        movementL.add(Movement.D);
        movementL.add(Movement.A);
        movementL.add(Movement.G);
        movementL.add(Movement.G);
        movementL.add(Movement.A);

        Assert.assertEquals(listA.get(0).getMovements(), movementL);

    }

    @Test
    public void writeFile() throws IOException {
        ParserFile parser = new ParserFile();
        parser.writeFile("/Users/quentin/Desktop/output.txt", explore);
    }

}
