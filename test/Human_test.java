package test;

import org.junit.Assert;
import org.junit.Test;

import classes.Coordinate;
import classes.Human;
import classes.Map;
import enums.Movement;
import enums.Direction;

public class Human_test {

    @Test
    public void testMoveClassic() {
        Map m = new Map(3, 4, null, null);
        Human quentin = new Human("Quentin", m, new Coordinate(2, 1), Direction.N, null);
        quentin.move(Movement.A);
        Assert.assertEquals(2, quentin.getCoordinate().getX());
        Assert.assertEquals(0, quentin.getCoordinate().getY());
        Assert.assertEquals(Direction.N, quentin.getOrientation());

        quentin.move(Movement.A);
        Assert.assertEquals(2, quentin.getCoordinate().getX());
        Assert.assertEquals(0, quentin.getCoordinate().getY());
        Assert.assertEquals(Direction.N, quentin.getOrientation());

        quentin.move(Movement.D);
        Assert.assertEquals(2, quentin.getCoordinate().getX());
        Assert.assertEquals(0, quentin.getCoordinate().getY());
        Assert.assertEquals(Direction.E, quentin.getOrientation());

        quentin.move(Movement.G);
        Assert.assertEquals(2, quentin.getCoordinate().getX());
        Assert.assertEquals(0, quentin.getCoordinate().getY());
        Assert.assertEquals(Direction.N, quentin.getOrientation());

        quentin.move(Movement.G);
        Assert.assertEquals(2, quentin.getCoordinate().getX());
        Assert.assertEquals(0, quentin.getCoordinate().getY());
        Assert.assertEquals(Direction.O, quentin.getOrientation());

        quentin.move(Movement.A);
        Assert.assertEquals(1, quentin.getCoordinate().getX());
        Assert.assertEquals(0, quentin.getCoordinate().getY());
        Assert.assertEquals(Direction.O, quentin.getOrientation());

        quentin.move(Movement.G);
        Assert.assertEquals(1, quentin.getCoordinate().getX());
        Assert.assertEquals(0, quentin.getCoordinate().getY());
        Assert.assertEquals(Direction.S, quentin.getOrientation());

        quentin.move(Movement.D);
        Assert.assertEquals(1, quentin.getCoordinate().getX());
        Assert.assertEquals(0, quentin.getCoordinate().getY());
        Assert.assertEquals(Direction.O, quentin.getOrientation());

    }

    @Test
    public void testMoveAngleRight() {
        Map m = new Map(3, 4, null, null);
        Human quentin = new Human("Quentin", m, new Coordinate(3, 0), Direction.E, null);
        quentin.move(Movement.A);
        Assert.assertEquals(3, quentin.getCoordinate().getX());
        Assert.assertEquals(0, quentin.getCoordinate().getY());
        Assert.assertEquals(Direction.E, quentin.getOrientation());

        quentin.move(Movement.G);
        Assert.assertEquals(3, quentin.getCoordinate().getX());
        Assert.assertEquals(0, quentin.getCoordinate().getY());
        Assert.assertEquals(Direction.N, quentin.getOrientation());

        quentin.move(Movement.G);
        Assert.assertEquals(3, quentin.getCoordinate().getX());
        Assert.assertEquals(0, quentin.getCoordinate().getY());
        Assert.assertEquals(Direction.O, quentin.getOrientation());
    }

    @Test
    public void testMoveAngleLeft() {
        Map m = new Map(3, 4, null, null);
        Human quentin = new Human("Quentin", m, new Coordinate(0, 0), Direction.N, null);
        quentin.move(Movement.G);
        Assert.assertEquals(0, quentin.getCoordinate().getX());
        Assert.assertEquals(0, quentin.getCoordinate().getY());
        Assert.assertEquals(Direction.O, quentin.getOrientation());

        quentin.move(Movement.G);
        Assert.assertEquals(0, quentin.getCoordinate().getX());
        Assert.assertEquals(0, quentin.getCoordinate().getY());
        Assert.assertEquals(Direction.S, quentin.getOrientation());

        quentin.move(Movement.A);
        Assert.assertEquals(0, quentin.getCoordinate().getX());
        Assert.assertEquals(1, quentin.getCoordinate().getY());
        Assert.assertEquals(Direction.S, quentin.getOrientation());
    }
}
