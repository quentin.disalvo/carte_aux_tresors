package test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import classes.Adventurer;
import classes.Coordinate;
import classes.ExploreMap;
import classes.Map;
import classes.ParserFile;
import classes.Treasure;
import enums.Direction;
import enums.Movement;

public class ExploreMap_test {
    List<Movement> movementQ = new ArrayList<>();
    List<Movement> movementM = new ArrayList<>();
    List<Movement> movementE = new ArrayList<>();

    Treasure t1 = new Treasure(new Coordinate(2, 3), 2);

    Map m = new Map(3, 4, null, null);

    Adventurer quentin = new Adventurer("Quentin", m, new Coordinate(0, 0), Direction.E, movementQ);
    Adventurer martin = new Adventurer("Martin", m, new Coordinate(1, 1), Direction.E, movementM);
    Adventurer etienne = new Adventurer("Etienne", m, new Coordinate(2, 2), Direction.O, movementE);

    ExploreMap explore = new ExploreMap(m, null);

    @Before
    public void init() {
        movementQ.add(Movement.A);
        movementQ.add(Movement.D);
        movementQ.add(Movement.A);
        movementQ.add(Movement.D);
        movementQ.add(Movement.A);

        movementM.add(Movement.D);
        movementM.add(Movement.A);
        movementM.add(Movement.D);
        movementM.add(Movement.A);
        movementM.add(Movement.A);

        movementE.add(Movement.G);
        movementE.add(Movement.A);
        movementE.add(Movement.G);
        movementE.add(Movement.A);
        movementE.add(Movement.D);
        movementE.add(Movement.D);
        movementE.add(Movement.A);

        Coordinate m1 = new Coordinate(1, 2);

        List<Coordinate> mountains = new ArrayList<>();
        mountains.add(m1);

        List<Treasure> treasures = new ArrayList<>();
        treasures.add(t1);

        m.setMountain(mountains);
        m.setTreasures(treasures);

        List<Adventurer> listExplorer = new ArrayList<>();
        listExplorer.add(etienne);
        listExplorer.add(quentin);
        listExplorer.add(martin);

        explore.setAdventurers(listExplorer);

    }

    @Test
    public void testColisionAdventurer() {

        explore.moveAdventurer(quentin.getMovements().get(0), quentin);
        Assert.assertEquals(1, quentin.getCoordinate().getX());
        Assert.assertEquals(0, quentin.getCoordinate().getY());
        Assert.assertEquals(Direction.E, quentin.getOrientation());

        explore.moveAdventurer(quentin.getMovements().get(1), quentin);
        Assert.assertEquals(1, quentin.getCoordinate().getX());
        Assert.assertEquals(0, quentin.getCoordinate().getY());
        Assert.assertEquals(Direction.S, quentin.getOrientation());

        explore.moveAdventurer(quentin.getMovements().get(2), quentin);
        Assert.assertEquals(1, quentin.getCoordinate().getX());
        Assert.assertEquals(0, quentin.getCoordinate().getY());
        Assert.assertEquals(Direction.S, quentin.getOrientation());

    }

    @Test
    public void testTreasure() {

        explore.moveAdventurer(etienne.getMovements().get(0), etienne);
        Assert.assertEquals(2, etienne.getCoordinate().getX());
        Assert.assertEquals(2, etienne.getCoordinate().getY());
        Assert.assertEquals(Direction.S, etienne.getOrientation());

        explore.moveAdventurer(etienne.getMovements().get(1), etienne);
        Assert.assertEquals(2, etienne.getCoordinate().getX());
        Assert.assertEquals(3, etienne.getCoordinate().getY());
        Assert.assertEquals(Direction.S, etienne.getOrientation());
        Assert.assertEquals(etienne.getTreasures(), 1);
        Assert.assertEquals(t1.getValue(), 1);

        explore.moveAdventurer(etienne.getMovements().get(2), etienne);
        Assert.assertEquals(2, etienne.getCoordinate().getX());
        Assert.assertEquals(3, etienne.getCoordinate().getY());
        Assert.assertEquals(Direction.E, etienne.getOrientation());
        Assert.assertEquals(etienne.getTreasures(), 1);
        Assert.assertEquals(t1.getValue(), 1);

        explore.moveAdventurer(etienne.getMovements().get(3), etienne);
        explore.moveAdventurer(etienne.getMovements().get(4), etienne);
        explore.moveAdventurer(etienne.getMovements().get(5), etienne);
        explore.moveAdventurer(etienne.getMovements().get(6), etienne);
        Assert.assertEquals(2, etienne.getCoordinate().getX());
        Assert.assertEquals(3, etienne.getCoordinate().getY());
        Assert.assertEquals(Direction.O, etienne.getOrientation());
        Assert.assertEquals(etienne.getTreasures(), 2);
        Assert.assertEquals(t1.getValue(), 0);

    }

    @Test
    public void testCollisionMountain() {

        explore.moveAdventurer(martin.getMovements().get(0), martin);
        Assert.assertEquals(1, martin.getCoordinate().getX());
        Assert.assertEquals(1, martin.getCoordinate().getY());
        Assert.assertEquals(Direction.S, martin.getOrientation());

        explore.moveAdventurer(martin.getMovements().get(1), martin);
        Assert.assertEquals(1, martin.getCoordinate().getX());
        Assert.assertEquals(1, martin.getCoordinate().getY());
        Assert.assertEquals(Direction.S, martin.getOrientation());

    }

    @Test
    public void testExplore() throws IOException {
        ParserFile parser = new ParserFile();
        ExploreMap explore2 = parser.readFile("/Users/quentin/Desktop/input2.txt");

        explore2.explore();
        parser.writeFile("/Users/quentin/Desktop/output.txt", explore2);
    }
}
